import json
import logging
import logging.config
import os
from pathlib import Path
from uuid import uuid4

from PIL import Image


file_names = []
pdf_file = "outposts.pdf"
log_file = "logging.json"
folder = Path(".")

# create logger
logger = logging.getLogger(__name__)
with open(log_file, "rt") as f:
    config = json.load(f)
    logging.config.dictConfig(config)


for count, imgfile in enumerate(folder.iterdir()):
    if imgfile.is_file():
        if imgfile.suffix == ".jpeg":
            file_names.append(Image.open(imgfile))

main_image = file_names[0]
other_images = file_names[1:]

main_image.save(pdf_file, "PDF", save_all=True, append_images=other_images)


### conversion: png not supported
# for file_name in file_names:
#     try:
#         img = Image.open(file_name)
#         img.save(file_name + "tiff")
#     except:
#         logger.error(f"unable to convert {file_name}")
#     else:
#         Path(file_name).unlink()
